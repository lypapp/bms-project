package com.abocode.jfaster.web.system.service;

import com.abocode.jfaster.core.common.service.CommonService;

/**
 * 
 * @author  张代浩
 *
 */
public interface MenuInitService extends CommonService{
	
	public void initMenu();
}
